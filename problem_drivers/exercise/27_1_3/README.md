# 27_1_3 +

## 题目

(C99)对 27.4 节的 quadratic.c 程序做如下修改:(c) 修改程序，使得虚部为负的复数的显示形式为 a-bi 而不是 a+-bi。

## 样例

### 样例一

Please Enter the value of a,b,c: -5,3,2

root1 = -0.4 + 0i
root2 = 1 - 0i

### 样例二

Please Enter the value of a,b,c: 3,-2,1

root1 = 0.33 + 0.47i
root2 = 0.33 - 0.47i

## 数据范围

输入输出的值均为'double'。
当需要保留小数位数时，请保留两位小数。