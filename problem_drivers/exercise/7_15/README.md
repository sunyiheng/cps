# 7_15 计算阶乘 Factorial calculate

## 题目

编程计算正整数的阶乘。

## 样例

### 样例一

    Enter a positive integer: 6
    Factorial of 6: 720

### 样例二

    Enter a positive integer: 0
    Factorial of 0: 1

## 数据范围

输入数据为小于 13 的非零整数。
