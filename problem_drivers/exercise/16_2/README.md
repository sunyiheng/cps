# 16_2 按序显示零件 Print inventory

## 题目

修改 16.3 节的 inventory.c 程序，使 p （显示）操作可以按零件编号的顺序显示零件。

## 样例

### 样例一

    Enter operation code: i
    Enter part number: 100
    Enter part name: Disk drive
    Enter quantity on hand: 50

    Enter operation code: i
    Enter part number: 50
    Enter part name: Printer
    Enter quantity on hand: 10

    Enter operation code: i
    Enter part number: 200
    Enter part name: Computer
    Enter quantity on hand: 20

    Enter operation code: s
    Enter part number: 100
    Part name: Disk drive
    Quantity on hand: 50

    Enter operation code: s
    Enter part number: 300
    Part not found.

    Enter operation code: u
    Enter part number: 100
    Enter change in quantity on hand: -2

    Enter operation code: u
    Enter part number: 50
    Enter change in quantity on hand: -2

    Enter operation code: s
    Enter part number: 100
    Part name: Disk drive
    Quantity on hand: 48

    Enter operation code: s
    Enter part number: 50
    Part name: Printer
    Quantity on hand: 12

    Enter operation code: p
    Part Number     Part Name          Quantity on Hand
        50            Printer               8
        100           Disk drive            48
        200           Computer              20

## 数据范围

输入的 Part number 为一个整数，Quantity on hand 也是整数。
Part number 和 Quantity on hand 的大小为 int 型。