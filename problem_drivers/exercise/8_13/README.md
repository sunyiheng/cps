# 8_3 修改7_11 Modify 7_11

## 题目

修改第 7 章的编程题 11，给输出加上标签。先显示姓氏（冒号后面需要带一个空格），其后跟一个逗号(逗号后面有一个空格)，然后显示名的首字母，最后加一个点。

## 样例

### 样例一

    Enter a first and last name： Lloyd Fosdick
    You entered the name: Fosdick, L.

### 样例二

    Enter a first and last name:  Jack Smith
    You entered the name: Smith, J.

## 数据范围

用户的输入中可能包含 0 - 2 个空格（名之前、名和姓之间、姓氏之后）。输入的姓和名都是以大写字母开头。

## 提示

在显示姓氏（不是名字）之前，程序需要将其存储在一个字符数组中。可以假定姓氏的长度不超过 20 个字符。
