# 16_4 添加零件价格 Add price to inventory

## 题目

修改 16.3 节的 inventory.c 程序，为结构 part 添加成员 price。insert 函数应该要求用户录入新商品的价格。search 函数和 print 函数
应该显示价格。添加一条新的命令，允许用户修改零件的价格。

## 样例

### 样例一

    Enter operation code: i
    Enter part number: 100
    Enter part name: Disk drive
    Enter quantity on hand: 50
    Enter price of part: 400

    Enter operation code: i
    Enter part number: 50
    Enter part name: Printer
    Enter quantity on hand: 10
    Enter price of part: 1000

    Enter operation code: i
    Enter part number: 200
    Enter part name: Computer
    Enter quantity on hand: 20
    Enter price of part: 2000

    Enter operation code: s
    Enter part number: 100
    Part name: Disk drive
    Quantity on hand: 50
    Price: 400

    Enter operation code: s
    Enter part number: 300
    Part not found.

    Enter operation code: u
    Enter part number: 100
    Enter change in quantity on hand: -2

    Enter operation code: s
    Enter part number: 100
    Part name: Disk drive
    Quantity on hand: 48

    Enter operation code: m
    Enter part number: 100
    Enter new price: 500

    Enter operation code: p
    Part Number     Part Name          Quantity on Hand     Price
        50            Printer               10               1000
        100           Disk drive            48               500
        200           Computer              20               2000

## 数据范围

修改价格时的 operation code 应该为 m 。新增加的 price 为 int 型。
