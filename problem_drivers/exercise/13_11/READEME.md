﻿### 第 13 章编程题 11

修改第 7 章的编程题 13，使其包含如下函数：

     double compute_average_word_length(const char *sentence);
函数返回 sentence 所指向的字符串中单词的平均长度。




