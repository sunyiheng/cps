#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define BUFFERSIZE 40
#define SIZE 99

int main(int argc, char *argv[])
{
	char buffer[BUFFERSIZE];
	memset(buffer, 0, BUFFERSIZE);
	int N = 0;
	printf("This program creates a magic square of a  \
specified size.\nThe size must be an odd   \
number bewween 1 and 99.\n");
	printf("Enter size of magic square: ");
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%d", &N);
	int magic[SIZE][SIZE] = {0};
	int length = N * N;
	int x_index = 0;
	int y_index = (N - 1)/2;
	int tempx = x_index;
	int tempy = y_index;
	for (int i = 0; i < length; i++)
	{
		magic[x_index][y_index] = i + 1;
        x_index--;
        y_index++;
        if (x_index < 0)
        	x_index += N;
        if (y_index == N)
        	y_index = 0;
        if (magic[x_index][y_index] > 0) {
	        x_index = tempx;
	        y_index = tempy;
	        x_index ++;
	    }
	    tempx = x_index;
	    tempy = y_index;
	}

	for (int i = 0; i < N; i++){
		for (int j = 0; j < N; j++){
			printf("%6d", magic[i][j]);
		}
		printf("\n");
	}
}