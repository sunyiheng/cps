# 7_10 统计元音字母个数

## 题目

编写程序统计句子中元音字母（a、e、i、o、u）的个数：

    Enter a sentence: And that's the way it is.
    Your sentence contains 6 vowels.

## 样例

### 样例一

    Enter a sentence: Please note, that we are planning on a partial reset/cleanup at the end of the alpha phase.
    Your sentence contains 30 vowels.

### 样例二

    Enter a sentence: And that's the way it is.
    Your sentence contains 6 vowels.

## 数据范围

1. 输入的数据应为一段英文;

## 提示

1. 按照题目内容中的输出为规范格式进行输出。