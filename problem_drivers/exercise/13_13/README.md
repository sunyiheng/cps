﻿### 第 13 章编程题 13

修改第 8 章的编程题 15，使其包含如下函数：

     void encrypt(char *message, int shift);
参数 message 指向一个包含待加密消息的字符串，shift 表示消息中每个字母需要移动的位数。



