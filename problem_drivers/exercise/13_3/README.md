# 13_3 随机发牌 Deal

## 题目

修改 8.2 节的 deal.c程序，使它显示出牌的全名。

## 样例

### 样例一

    Enter number of cards in hand: 5
    Your hand:
    Seven of clubs
    Two of spades
    Five of diamonds
    Ace of spades
    Two of hearts

### 样例二

    Enter number of cards in hand: 6
    Your hand:
    Four of Diamonds
    Nine of Clubs
    Nine of Spades
    King of Clubs
    Three of Diamonds
    Ace of Clubs

## 数据范围

输入的牌的数量不超过牌的总数即 52 张。

## 提示

用指向字符串的指针的数组来替换数组 rank_code 和数组 suit_code。