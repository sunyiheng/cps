# 13_5 对命令行参数求和 Sum arguments

## 题目

编写名为 sum.c 的程序，用来对命令行参数（假设都是整数）求和。

## 样例

### 样例一

输入：

    sum 8 24 62

输出：

    Total： 94

### 样例二

输入：

    sum 4 87 90

输出：

    Total： 181

## 数据范围

输入的数大小均为 int 型，输出的结果大小也为 int 型。命令行参数
仅允许输入数字。命令行参数中的数据不局限于样例中的个数。

## 提示

用 atoi 函数把每个命令行参数从字符串格式转换为整数格式。