#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#define BUFF_SIZE 40

int get_line(char *);

int main()
{
	const int value[] = 
	{1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5,
	 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4,
	 4, 8, 4, 10};
    /* int val; */
    printf("Enter a word: ");
    char buffer[BUFF_SIZE];
    int length = get_line(buffer);
    /* printf("%s\n",buffer); */
    int result = 0;
    for(int i = 0; i < length; i++){
        /* val = tolower(buffer[i]); */
        result += value[(int)((tolower(buffer[i])) - 'a')];
    }
	printf("Scrabble value: %d\n", result);
    return 0;
}

int get_line(char *s){
    int c;
    int length = 0;
    while((c = getc(stdin)) != '\n'){
        *s++ = c;
        length++;
    }
    *s = '\0';
    return length;
}
