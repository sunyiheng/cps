#include <stdio.h>
#include <string.h>
#define BUFFERSIZE 40
#define MAXSIZE 40

int main(int argc, char *argv)
{
	char buffer[BUFFERSIZE];
	memset(buffer, 0, BUFFERSIZE);
	int length;
	char number[MAXSIZE];
	char temp;

    printf("Enter a two-digit number: ");
	fgets(buffer, sizeof(buffer), stdin);
    length = strlen(buffer);
    buffer[length] = '\0';
    sscanf(buffer, "%[0-9]", number);
    length = strlen(number);
    number[length] = '\0';
    for(int i = 0, j = length - 1; i < j; i++, j--)
    {
    	temp = number[i];
    	number[i] = number[j];
    	number[j] = temp;
    }
    printf("The reversal is: %s\n", number);

}
