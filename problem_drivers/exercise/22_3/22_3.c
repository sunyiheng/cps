#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    FILE *fp;

    if (argc < 2) {
        printf("Usage: programname filename(s).\n");
        exit(EXIT_FAILURE);
    }

    int flag = 0; //the number of file that can not be opened
    char buff[100] = "can not open file:";
    
    int i; 
    for ( i = 1; i < argc; ++i) {
        if ((fp = fopen(argv[i], "r")) == NULL) {
            flag++;
            strcat(buff, " ");
            strcat(buff, argv[i]);
        }
        else {
            printf("%s", argv[i]);
            fclose(fp);
        }
    }
    printf("\n");
    
    if (flag > 0)
    	printf("%s.\n", buff);
    return 0;
}
