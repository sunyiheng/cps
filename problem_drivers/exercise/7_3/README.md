# 7-3 修改 sum2.c Modify sum2.c：

## 题目

修改 7.1 节的程序 sum2.c，对 double 型值组成的数列求和。

## 样例

### 样例一

    This program sums a series of integers.
    Enter integers (0 to terminate): 234.9 23.0 21.233 4.2 0
    The sum is: 283.333000

### 样例二

    This program sums a series of integers.
    Enter integers (0 to terminate): 23.9 -34.2 45 0
    The sum is: 34.700000

## 数据范围

假设所有数据都在 double 范围内
