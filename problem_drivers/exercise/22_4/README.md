# 22_4 统计文件行数、单词数、字符数 Calculate lines words characters

## 题目

(a) 编写程序统计文本文件中字符的数量。
(b) 编写程序统计文本文件中单词的数量。（所谓“单词”指的是不含空白字符的任意序列）
(c) 编写程序统计文本文件中行的数量。
要求每一个程序都通过命令行获得文件名。

## 样例

### 样例一

文件 test 的内容如下：

    hello. word
    basketball

输入：

    calculate test

输出：

    Characters in the file "test": 23
    Words in the file "test": 3
    Lines in the file "test": 2

## 数据范围

文件中可以出现任意的字符。统计时除文件末尾的结束符可以不统计，但其他特殊字符，如：
空白、换行、制表符等都应统计在内。对于单词的定义：只要给定一个字符串，且该字符串
无空白字符都可以称为单词，如 "hello" 和 "hello." 都是单词。