# 6_12 计算 e 的近似值(改 6.12)

## 题目

修改编程题 11，使得程序持续执行加法运算，直到当前项小于 ε 为止，其中 ε 是用户输入的较小的（浮点）数。

## 样例

### 样例一

    Enter a small double floating point number: 0.0001
    result is: 2.718254

### 样例二

    Enter a small double floating point number: 0.00000000000001
    result is: 2.718282

## 数据范围

1. 输入的数据应为double类型而不是float类型;

## 提示

1. 按照题目内容中的输出为规范格式进行输出。