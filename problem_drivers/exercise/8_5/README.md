# 8_5 修改 interest.c Modify interest.c

## 题目

修改 8.1 节的程序 interest.c ，使得修改后的程序可以每月整合一次利息，而不是每年整合一次利息。不要改变程序的输出格式，余额仍按每年一次的时间间隔显示。

## 样例

### 样例一

    Enter interest rate: 2
    Enter number of years: 5

    Years     2%     3%     4%     5%     6%
    1     102.02 103.04 104.07 105.12 106.17
    2     104.08 106.18 108.31 110.49 112.72
    3     106.18 109.41 112.73 116.15 119.67
    4     108.32 112.73 117.32 122.09 127.05
    5     110.51 116.16 122.10 128.34 134.89

### 样例二

    Enter interest rate: 3
    Enter number of years: 6

    Years     3%     4%     5%     6%     7%
    1     103.04 104.07 105.12 106.17 107.23
    2     106.18 108.31 110.49 112.72 114.98
    3     109.41 112.73 116.15 119.67 123.29
    4     112.73 117.32 122.09 127.05 132.21
    5     116.16 122.10 128.34 134.89 141.76
    6     119.69 127.07 134.90 143.20 152.01

## 数据范围

假设输入的利率和年份都为整数。