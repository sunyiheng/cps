import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AutoGrading {
    private String resultPath = "resource/result_bak.txt";
    private String source = "resource/shall_redirect_bak.c";
    public static String lineSeparator = System.lineSeparator();  // 获得系统的换行符

    public String getResultPath() {
        return resultPath;
    }
    public String getSource() {
        return source;
    }

    public AutoGrading() { }

    public AutoGrading(String source, String resultPath) {
        this.resultPath = resultPath;
        this.source = source;
    }

    /**
     * 针对C语言：
     *  提取目标代码中，printf("xxx", %x)中的字符串 "xxx"，
     *  其中"xxx"的形式可能为 "abc: %-3.2f, efg: %c"这样的待处理形式
     * @return 被提取出的printf字符串
     */
    public List<String> getPrintContent() throws IOException {
        // 声明一个ArrayList 用以存放最后提取出的"xxx" 字符串，交给下一步处理
        List<String> list = new ArrayList<>();

        /*
         * 读出文件为字符串，并且将文本中的换行符统一为\n
         */
        File file = new File(source);
        String fileContent = FileUtils.readFileToString(file, "utf-8").replaceAll("\r\n|\r", "\n");

        /*
         * 用正则表达式，从文件字符串中提取出printf("xxx", %x);
         */
        String regex = "printf\\(.*\\);";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(fileContent);

        /*
         *  构造提取出printf("xxx", %d) 中的"xxx"字符串的模式
         */
        String strInPrintRegex = "\".*\"";
        Pattern strPattern = Pattern.compile(strInPrintRegex);

        /*
         * 1.每找到一个printf("xxx", %x); 字符串，就构造一个matcher，从printf("xxx", %x); 中提取出"xxx"字符串
         * 2.提取出"xxx"字符串之后，去掉字符串中开头和结尾的引号: "
         */
        while(matcher.find()) {
            // 创建一个从printf("xxx", %x);中提取"xxx"的Matcher
            Matcher strMatcher = strPattern.matcher(matcher.group());
            // 每找到一个"xxx"字符串，就提取出来并且去掉字符串开头和结尾的引号
            while(strMatcher.find()) {
                // 匹配一个"xxx"字符串
                String temp = strMatcher.group();
                // 去掉前引号
                if(temp.startsWith("\"")) {
                    temp = temp.substring(1);
                }
                // 去掉后引号
                if(temp.endsWith("\"")) {
                    temp = temp.substring(0,temp.length() - 1);
                }
                // 将提取出来的字符串添加到待处理的模板列表中
                list.add(temp);
            }
        }
        return list;
    }

    /**
     * 将getPrintContent()方法得到的形如："abc: %-3.2f, efg: %c"的字符串进行处理
     * 处理方式：将字符串，以"%c"等形式 进行切割，如："I have %d dollars and %d bills rest"，
     *          切割将被切割为"I have "、" dollars and "、" bills rest"三个字符串
     *          这些字符串就是需要被从结果字符串中剔除的字符串的模板
     */
    public List<String> getPrintTemplate() throws IOException {

        // 声明一个列表，用以存放模板
        List<String> template = new ArrayList<>();

        // 得到待处理的"xxx%dxxx"的字符串
        List<String> printContent = getPrintContent();

        // 构造能匹配 %c/s 或 %2d 或 %.2f 或 %3.2f 或 %-3.2f 等样式的的正则表达式
        String split = "(?<!\\\\)%[+-]?([0-9]?(\\.)?)?[0-9]?[c|s|d|f]";

        /*
         * 迭代列表中的每个待处理的"xxx%dxxx"字符串"
         *
         */
        for(String eachContent : printContent) {
            // 分割字符串，将得到字符串暂时存入temp数组
            String[] temp = eachContent.split(split);
            /*
             * 循环迭代temp数组，将得到的模板添加进template列表。
             * 若：1. template中已经存在该模板，则不重复添加
             *    2. 如果是因切割产生的空字符串，也跳过，不添加
             */
            for(String eachTemplate : temp) {
                // 遇到空字符串，或者template中已存在该模板，则跳过，不添加
                if(eachTemplate.equals("") || template.contains(eachTemplate)) {
                    continue;
                }

                // 将printf的字符串中的"\n"替换成真正的换行符，并添加进模板中
                if(eachTemplate.contains("\\n")) {
                    eachTemplate = eachTemplate.replace("\\n", "\n");
                }
                template.add(eachTemplate);
            }
        }
        // 像模板列表中添加"\n"换行符，作为模板
        if(!template.contains("\n")) {
            template.add("\n");
        }
        // 除去模板列表中所有的空字符串
        while(template.contains("")) {
            template.remove("");
        }

        return template;
    }

    /**
     * 1.通过得到的模板列表，将结果文件中，所有匹配模板的子串给去除，由此得到输出结果序列
     * 2.并调整序列的格式，使最终结果序列的每个结果以"|"符号分隔开
     */
    public String getResultSequence() throws IOException {
        // 得到模板列表
        List<String> template = getPrintTemplate();
        // 读出结果文件为字符串，并统一换行符
        String fileStr = FileUtils.readFileToString(new File(resultPath), "utf-8").replaceAll("\r\n|\r", "\n");

        // 对模板列表迭代，消除结果文件字符串中所有与模板匹配的子串，消除后，留下一个"|"占位，方便调整格式
        for (String eachTemplate : template) {
            fileStr = fileStr.replace(eachTemplate, "|");
        }
        // 将连续冗余的占位的"|"除去
        fileStr = fileStr.replaceAll("[|]+", "|");
        // 去掉开头和结尾的 "|"
        if(fileStr.startsWith("|")) {
            fileStr = fileStr.substring(1);
        }
        if(fileStr.endsWith("|")) {
            fileStr = fileStr.substring(0,fileStr.length() - 1);
        }

        return fileStr;
    }

    /**
     * 打分主函数，打分细则如下：
     *  1.进入该打分程序，则说明编译已经通过，则已经有20分的基础分
     *
     *  2.用例测试（70分），测试方法如下：
     *      0> 对待测结果文本提取出真正有用的结果序列
     *      1> 使最终结果序列的每个结果以"|"符号分隔开
     *      2> 对标准结果文本进行同0-1步的操作，得到标准结果序列
     *      3> 将待测结果序列每一个结果行与标准结果文本的每个结果行进行比较：每正确一个，则得到该结果的分
     *
     *  3.格式比对（10分），将待测文本与标准文本进行完全比对，如果完全匹配，则满分。
     */
    public static void main(String[] args) {
        /*
         * 第一个参数：标准答案源代码
         * 第二个参数：标准答案的结果输出文本
         * 第三个参数：待测试的源代码
         * 第四个参数：待测试的结果输出文本
         */
        if(args.length != 4) {
            System.out.println("输出参数个数有误");
            return;
        }
        AutoGrading standard = new AutoGrading(args[0], args[1]);
        AutoGrading test = new AutoGrading(args[2], args[3]);


        try {
            // 将标准答案的结果文本与待测试的结果文本，读出为字符串，并且统一换行符
            String stdResult = FileUtils.readFileToString(new File(standard.getResultPath()), "utf-8").replaceAll("\r\n|\r","\n");
            String tstResult = FileUtils.readFileToString(new File(test.getResultPath()), "utf-8").replaceAll("\r\n|\r","\n");

            // 如果两个文本的内容完全一致，则满分
            if(stdResult.equals(tstResult)) {
                System.out.println("{\"scores\": {\"Correctness\": 100}}");
                return;
            }

            // 分别得到两个输出文本的结果序列
            String standardResultSequence = standard.getResultSequence();
            String testResultSequence = test.getResultSequence();

            // 将结果序列以"|"划分
            String[] stdResultSeqArr = standardResultSequence.split("|");
            String[] tstResultSeqArr = testResultSequence.split("|");

            // 如果标准结果的个数与待测结果的个数不一致，则无法比对答案
            int resultSeqLen = stdResultSeqArr.length;
            if(resultSeqLen != tstResultSeqArr.length) {
                System.out.println("{\"scores\": {\"Correctness\": 20}}");
                return;
            }

            // 得出正确结果的个数
            int correct = 0;
            for(int i = 0; i < resultSeqLen; i++) {
                if(stdResultSeqArr[i].equals(tstResultSeqArr[i])) {
                    correct++;
                }
            }

            // 每正确一个，则得到相应的分数
            int score = 20 + correct / resultSeqLen * 70;
            System.out.println("{\"scores\": {\"Correctness\": " + score + "}}");
            return;

        } catch (IOException e) {
            System.out.println("读取文件失败，请检查文件名是否正确！");
            return;
        }

    }

}
