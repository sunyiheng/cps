# 6_6 打印偶数的平方值 Print the square of even numbers

## 题目

编写程序，提示用户输入一个数 n，然后显示出 1~n 的所有偶数的平方。

## 样例

### 样例一

    Enter a number: 100
    4
    16
    36
    64
    100

### 样例二

    Enter a number: 50
    4
    16
    36

## 数据范围

输入 n >= 1