#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>

#define STACK_SIZE 100

bool isFull();
bool isEmpty();
void stackOverflow();
void stackUnderflow();
void push(char);
char pop();

char contents[STACK_SIZE];
int top = 0;

bool isFull(){
    return top == STACK_SIZE;
}

bool isEmpty(){
    return top == 0;
}

void stackOverflow(){
    printf("Stack overflow\nParenteses/braces are not nested properly\n");
    exit(EXIT_FAILURE);
}

void stackUnderflow(){
    printf("Stack overflow\nParenteses/braces are not nested properly\n");
    exit(EXIT_FAILURE);
}

void push(char c){
    if(isFull())
        stackOverflow();
    else
        contents[top++] = c;
}

char pop(void){
    if(isEmpty()){
        stackUnderflow();
        return 0;
    }
    else
        return contents[--top];
}

int main(){
    char string[STACK_SIZE];
    char c;
    char valid[] = "Parenteses/braces are nested properly\n";
    char invalid[] = "Parenteses/braces are not nested properly\n";    
    printf("Enter parenteses and/or braces: ");
    while((c = getc(stdin)) != '\n'){
        if((c == '(') || (c == '{')){
            push(c);
        }
        else if(c == ')'){
            if(pop() != '('){
                printf("%s",invalid);
                return 1;
            }
        }
        else if(c == '}'){
            if(pop() != '{'){
                printf("%s",invalid);
                return 1;
            }
        }
    }
    printf("%s",isEmpty() ? valid : invalid);
    return 0;    
}

