# 随机步法 (Random footwalk)

## 题目编号：8-9

## 题目名称：

编写程序,生成一种贯穿10×10字符数组（初始时全为字符’.）的“随机步法”。程序必须随机地从一个元素“走到”另一个元素,每次都向上、向下、向左或向右移动一个元素位置。已访问过的元素按访问顺序用字母A到Z进行标记。下面是一个输出示例:

        A . . . . . . . . .
        B C D . . . . . . .
        . F E . . . . . . .
        H G . . . . . . . .
        I . . . . . . . . .
        J . . . . . . . Z .
        K . . R S T U V Y .
        L M P Q . . . W X .
        . N O . . . . . . .
        . . . . . . . . . .

    下面是提前结束的一个示例:

        A B G H I . . . . .
        . C F . J K . . . .
        . D E . M L . . . .
        . . W X Y P Q . . .
        . . V U T S R . . .
        . . . . . . . . . .
        . . . . . . . . . .
        . . . . . . . . . .
        . . . . . . . . . .

    因为Y的4个方向都堵住了,所以没有地方可以放置下一步的Z了。

## 输出范例：

### 范例一

        A . . . . . . . . .
        B C D . . . . . . .
        . F E . . . . . . .
        H G . . . . . . . .
        I . . . . . . . . .
        J . . . . . . . Z .
        K . . R S T U V Y .
        L M P Q . . . W X .
        . N O . . . . . . .
        . . . . . . . . . .

### 范例二

        A B G H I . . . . .
        . C F . J K . . . .
        . D E . M L . . . .
        . . W X Y P Q . . .
        . . V U T S R . . .
        . . . . . . . . . .
        . . . . . . . . . .
        . . . . . . . . . .
        . . . . . . . . . .

## 数据范围

按照题目的规定随机进行输出。