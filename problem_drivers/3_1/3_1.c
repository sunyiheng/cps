#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define BUFSIZE 128

int main(int argc, char *argv[])
{
    printf("Enter phone number: ");
    char buf[BUFSIZE];
    int length;
    char ch;
    fgets(buf, sizeof(buf), stdin);
    length = strlen(buf);
    int i;
    for (i = 0; i < length; i++)
    {
        ch = buf[i];
        switch (ch)
        {
            case 'A':case 'B': case 'C': buf[i] = '2';break;
            case 'D':case 'E': case 'F': buf[i] = '3';break;
            case 'G':case 'H': case 'I': buf[i] = '4';break;
            case 'J':case 'K': case 'L': buf[i] = '5';break;
            case 'M':case 'N': case 'O': buf[i] = '6';break;
            case 'P':case 'Q': case 'R':case 'S': buf[i] = '7';break;
            case 'T':case 'U': case 'V': buf[i] = '8';break;
            case 'W':case 'X': case 'Y':case 'Z': buf[i] = '9';break;
            default: buf[i] = ch; break;
        }
    }
    printf("%s", buf);
    return 0;
}
