# 3_3 显示次数 Number of display

## 题目

修改 8.1 节的程序 repdigit.c, 使其打印一份列表，显示出每个数字在其中出现的次数。

## 样例

### 样例一

    Enter a number: 41271092
    Digit:        0  1  2  3  4  5  6  7  8  9
    Occurrences:  1  2  2  0  1  0  0  1  0  1

### 样例二

    Enter a number: 1234567890
    Digit:        0  1  2  3  4  5  6  7  8  9
    Occurrences:  1  1  1  1  1  1  1  1  1  1

## 数据范围

按照规范格式进行输入输出。输入的数字范围从 0 到 2147483647。