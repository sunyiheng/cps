#include <stdio.h>
#include <string.h>

#define MAXSIZE 1000
int main(void)
{
    int no_of_digits_seen[10] = {0};
    char digit[MAXSIZE];
    long n;

    printf("Enter a number: ");
    scanf("%s\n", digit);
    int length = strlen(digit);
    for(int i = 0; i < length; ++i) 
        ++no_of_digits_seen[digit[i] - '0'];

    printf("%-14s0  1  2  3  4  5  6  7  8  9\n", "Digit:");
    printf("%-14s", "Occurrences:");
    for (n = 0; n < 10; n++) {
        printf("%-3d", no_of_digits_seen[n]);
    }

    return 0;
}
